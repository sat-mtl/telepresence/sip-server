Release Notes
===================

SIP Server 2.2.0 (2023-06-05)
----------------------------------

* ✨ Add SIP reflector service and fix re-registration
* Use Switcher 3.1.13 for SIP integration test
* Add SignalWire access token argument SIGNALWIRE_TOKEN

SIP Server 2.1.1 (2022-02-16)
----------------------------------
 
* 🐛 Fix/phpLDAPadmin env variables
* 🧑‍💻 Add Feature Request and Bug Resolution Request templates

SIP Server 2.1.0 (2022-01-12)
-----------------------------

Bug fixes:
* Flush zombie calls on SIP registration expire event
* Block SIP requests not addressed to domain via TCP transport

New features:
* Add backup SIP port 9060
* Add coturn telnet CLI interface

Improvements:
* Upgrade coturn from 4.5.0.5 (2020-12-15) to 4.5.2-3 (2021-03-30)
* Moved coturn dockerfile in sip-server repo
* Use coturn docker image from registry.gitlab.com
* Configure coturn logging rotation on /var/log/coturn/turn.log
* Configure FreeSWITCH logging rotation on /var/log/freeswitch/freeswitch.log
* Configure pruning logs older than 30 days
* Configure Docker logging rotation and pruning at 500m per service
* Upgrade Docker Compose from 1.25.1 to v2.1.0
* Upgrade SIP test to Switcher 3.0.5

SIP Server 2.0.0 (2021-09-20)
-----------------------------

New features:
* Add SIP firewall allowing only REGISTER and INVITE containing domain name
* Add FreeSWITCH fs_cli interface
* Add Switcher SIP call test to CI
* Add CI/CD pipeline

Improvements:
* Upgrade FreeSWITCH to 1.10.6 from source install
* Configure FreeSWITCH standard log levels
* Moved FreeSWITCH dockerfile in sip-server repo
* Use FreeSWITCH docker image from registry.gitlab.com
* Add FreeSWITCH command switches -nonat and -rp
* Add sys_nice capability to FreeSWITCH docker container to allow re-nice real-time process priority
* Drop FS to freeswitch user and group
* Map freeswitch UID and GID 10000 to avoid host users clash
* Add limits.d file recommended by upstream FreeSWITCH dockerfile

Documentation:
* Add fs_cli usage example in README
* Add sngrep package and example use in README to debug SIP clients
* Add info about sip firewall in README

SIP Server 1.0.0 (2020-04-02)
---------------------------------

* 🐛 Fix init_domain script
* 📝 Rewrite README
* 🐳 Fix typo in docker-compose.yml
* 📄 Convert LICENSE to Markdown
* 📝 Add AUTHORS and CODEOWNERS
* 🙈 Update .gitignore

SIP Server 0.0.1 (2020-04-02)
---------------------------------

* 🎉 Initial state of the forked repo
