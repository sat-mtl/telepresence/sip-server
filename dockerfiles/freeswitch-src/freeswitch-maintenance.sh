#!/bin/sh
days_keep_logs=30

find /var/log/freeswitch/freeswitch.log.* -mtime +$days_keep_logs -exec rm {} \;