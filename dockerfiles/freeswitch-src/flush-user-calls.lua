-- flush-user-calls.lua
-- Flush active calls for a user when called from sofia::expire event hook.
-- List active channels for event's username and kill all related calls.

--[[
Function to kill all calls in channels table.
--]]

-- channels = table containing decoded json output of "show channels" API command

function flushCalls(channels)
    if type(channels) == "table" then
        local i = channels["row_count"]
        freeswitch.consoleLog("INFO","flush-user-calls.lua: number of channels: " .. i .. "\n")

        while (i > 0)
        do
            local call_uuid = channels["rows"][i]["call_uuid"]
            freeswitch.consoleLog("INFO","flush-user-calls.lua: killing call_uuid: " .. call_uuid .. "\n")

            local cmd = "uuid_kill " .. call_uuid
            local channels_json = api:executeString(cmd)

            i = i-1
        end
    end 
end -- flushCalls

local cjson = require "cjson"
api = freeswitch.API()

freeswitch.consoleLog("INFO","flush-user-calls.lua called.\n")

-- Display all event data for debug
--event_data = event:serialize()
--freeswitch.consoleLog("INFO","flush-user-calls event data: \n" .. event_data .. "\n")

-- Fetch which user we should flush calls
local event_username = event:getHeader("username")
freeswitch.consoleLog("INFO","flush-user-calls.lua: user we want to flush calls: " .. event_username .. "\n")

-- Hangup user's calls
local cmd = "show channels like " .. event_username .. " as json"
local channels_json = api:executeString(cmd)
local channels = cjson.decode(channels_json)
flushCalls(channels)